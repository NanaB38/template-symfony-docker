<?php

namespace App\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\DoctrineProductRepository")
 *
 * @ORM\Table(name="catalog_product")
 */
class Product
{

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     *
     */
    private string $id;

    /**
     * @ORM\Column(name="code", type="string")
     */
    private string $code;

    /**
     * @ORM\Column(name="name", type="string", length=180)
     */
    private string $name;

    /**
     * @ORM\Column(name="stock", type="integer")
     */
    private int $stock = 0;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\Column(name="description_short", type="text", nullable=true)
     */
    private ?string $descriptionShort = null;

    /**
     * @ORM\Column(name="image", type="text", nullable=true)
     */
    private ?string $image = null;

    public function __construct()
    {

    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

}