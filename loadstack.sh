#environnement
STACKNAME="peexeo"
PHP_CONTAINER="${STACKNAME}_php_1"
NGINX_CONTAINER="${STACKNAME}_nginx_1"

#build stack
docker-compose -p $STACKNAME up -d --build --force-recreate
#docker-compose -p peexeo up -d --build --force-recreate

#log into application php container
docker exec -ti $PHP_CONTAINER bash
#docker exec -ti peexeo_php_1 bash
